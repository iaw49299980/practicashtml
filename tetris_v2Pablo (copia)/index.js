/*
 * JS        08/11/2018
 *
 * Moviment de peces
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */
 // Inicialització de variables globals
 var mare;
 var nombreElementsMare = 0;
 var element;
 var x = 0;
 var y;
 var input;
 var posicionsOcupades;
 var rowArray;
 var columnArray;
 var elemX;
 var elemY;
 // Assignació de valors a les variables globals
 function ini() {
   mare = document.getElementById("mare");
   input = document.getElementById("manejador");
   posicionsOcupades =
            [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]];
	document.onkeydown=eventoTeclado;
	veureArray();
 }

//event.keyCode === 13 intro
// left = 37
//up = 38
//right = 39
//down = 40

function eventoTeclado(event) {
	if (window.event) {
		keynum = window.event.keyCode;
	} else if (event.which) {
		keynum = event.which;
	}
	if (keynum == 13) {
		crearDiv();
	}
	else if (event.keyCode === 37) {
		mover(-1, 0);
	}
	else if (event.keyCode === 39) {
		mover(1, 0, elemArray);
	}
	else if (event.keyCode === 38) {
		mover(0, -1);
	}
	else if (event.keyCode === 40) {
		mover(0, 1);
	}
	else if (event.keyCode === 32) {
		 while (mover(0, 1)) {
			mover(0, 1);
		}

	}
}


function crearDiv() {
  randomCaja = Math.floor(Math.random()*3);
	if (posicionsOcupades[0][0] != 1) {
		mare.innerHTML += "<div class='capsa"+ randomCaja +"' id='c" + nombreElementsMare + "'></div>";
		element = document.getElementById('c'+nombreElementsMare);
		nombreElementsMare++;
		rowArray = 0;
		columnArray = 0;
    if (randomCaja == 0) {
        elemArray = [[1]];
    } else if (randomCaja == 2) {
      elemArray = [[1, 1, 1]];
    } else {
      elemArray = [[1, 1, 1],
                  [1, 1, 1],
                  [1, 1, 1]];
    }
    for (i = 0; i < elemArray.length; i++) {
      for (j = 0; j < elemArray[0].length; j++){
		    posicionsOcupades[i][j] = 1;
      }
    }
	}
	veureArray();
}
function mover(xF, yF) {
  for (i = 0; i < elemArray.length; i++) {
    for (j = 0; j < elemArray[0].length; j++){
      posicionsOcupades[i + columnArray][j + rowArray] = 0;
    }
  }
  ocupada = false;
	columnArray += yF;
  rowArray += xF;
  for (i = 0; i < elemArray.length && !ocupada; i++) {
    for (j = 0; j < elemArray[0].length && !ocupada; j++){
      if (posicionsOcupades[i + columnArray]) {
        ocupada = posicionsOcupades[i + columnArray][j + rowArray] == 1 ? true : false;
      } else {
        ocupada = true;
      }
    }
  }
	if (ocupada || !posicionsOcupades[columnArray] || !posicionsOcupades[columnArray]
    || posicionsOcupades[columnArray][rowArray] == 1
    || rowArray == -1
    || rowArray + elemArray[0].length - 1 == posicionsOcupades[0].length) {
    columnArray -= yF;
    rowArray -= xF;
    for (i = 0; i < elemArray.length; i++) {
      for (j = 0; j < elemArray[0].length; j++){
        posicionsOcupades[i + columnArray][j + rowArray] = 1;
      }
    }
    return false;
	} else {
    if (xF == 1) {
      x = element.offsetLeft + 50;
  		element.style.left = x + "px" ;
    } else if (xF == -1) {
      x = element.offsetLeft - 50;
  		element.style.left = x + "px" ;
    } else if (yF == -1) {
      x = element.offsetTop - 50;
  		element.style.top = x + "px" ;
    } else if (yF == 1) {
      x = element.offsetTop + 50;
  		element.style.top = x + "px" ;
    }
    for (i = 0; i < elemArray.length; i++) {
      for (j = 0; j < elemArray[0].length; j++){
		    posicionsOcupades[i + columnArray][j + rowArray] = 1;
      }
    }
    esPotMoure = true;
	}
	veureArray();
  return esPotMoure;
}
function veureArray() {
	divArray = document.getElementById("veureArray");
	divArray.innerHTML = "";
	for (var i = 0; i < posicionsOcupades.length; i++) {
		for (var j = 0; j < posicionsOcupades[0].length; j++) {
			divArray.innerHTML += "<div class='divArrayInfo'><p>"+ posicionsOcupades[i][j] +"</p></div>";

		}
	}

}
