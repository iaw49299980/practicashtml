/*
 * JS        08/11/2018
 *
 * Programa que fa el funcionament del joc qui és qui
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 * 
 */
var idAnterior; // Iniciem la variable que en guardará la id anterior
var parella = 0; // Iniciem la variable que ens dirá si em clickat en dues imatges o només portem una.
var errades = 0; // Iniciem la variable que ens portará les errades.
var encerts = 0; // Iniciem la variable que ens portará els encerts
var contador = localStorage.length; // Iniciem la variable de classificació.
 /**
     * 
     * Funció que al fer load a la pàgina reinicia el contador d'errades i encerts.
     * 
     */
function marcador() { 
	document.getElementById('encerts').value = 0; 
	document.getElementById('errades').value = 0;
}

 /**
     * 
     * Funció que donat l'id fa un display block a l'imatge clickada, i en cas de ser la segona
     * passa el valor de l'id actual i de l'anterior a la funció comprovarParella().
     * 
     * @param id
     * 
	 */

function cambiaImatge(id) {
		// Agafem l'element clickat
		var img = document.getElementsByTagName("img")[id].style.display = "block"; 
		// Comprovem que no sigui el primer moviment de la partida, si no ho és
		// li passem l'element clickat i l'anterior per comparar-los
		if (idAnterior != undefined) {
			comprovarParella(id, idAnterior);
		} else {
			parella++;
		}
		idAnterior = id;
}
 /**
     * 
     * Funció que donat l'id de les dues imatges a comparar, ens comprova que siguin iguals.
     * Si són iguals l'hi treu el event list als elements i suma un a encerts, també comprova
     * si portem em encertat totes. En cas contrari, suma una errada i amb un delay de 500 milisegons
     * torna invisible les dues imatges.
     * 
     * 
     * @param id
     * @param idAnterior
     * 
	 */
function comprovarParella(id, idAnterior) {
		parella++;
		if (parella % 2 == 0) {
			var img = document.getElementsByTagName("img")[id];
			var imgAnterior = document.getElementsByTagName("img")[idAnterior];
			if (img.src == imgAnterior.src) {
				var divImg = document.getElementsByClassName("caja")[id];
				var divAnterior = document.getElementsByClassName("caja")[idAnterior];
				divImg.removeAttribute("onclick");
				divImg.style.border="5px solid green";
				divAnterior.removeAttribute("onclick");
				divAnterior.style.border="5px solid green";
				encerts++;
				document.getElementById("encerts").value = encerts; 
				if (encerts == 4) {
					alert("Molt bé. Només has hagut de fer " + errades + " errades");
					document.getElementById("classificacio").style.display = "block";
					document.getElementById("contenedor").style.opacity = "0.5";
					document.getElementById("resultat").style.opacity = "0.5";
				}
			} else {
				errades++;
				document.getElementById("errades").value = errades; 
				setTimeout(
					function() { 
						img.style.display = "none";
						imgAnterior.style.display = "none";
					}, 400);
					
			}
		} 
}
 /**
     * 
     * Funció que donat l'id de les dues imatges a comparar, ens comprova que siguin iguals.
     * Si són iguals l'hi treu el event list als elements i suma un a encerts, també comprova
     * si portem em encertat totes. En cas contrari, suma una errada i amb un delay de 500 milisegons
     * torna invisible les dues imatges.
     * 
     * 
     * @param id
     * @param idAnterior
     * 
	 */
function setResultat(input) {
	var usuari = document.getElementById("nom").value;
	var llistaClassificacio = document.getElementById("llistaClassificacio");
	localStorage.setItem('usuari' + contador, usuari);
	localStorage.setItem('resultado' + contador, errades);
	llistaClassificacio.innerHTML += "<h3>Resultats</h3>"
	llistaClassificacio.innerHTML += "<th>Usuari</th><th>Errades</th>"
	for (var i = 0; i < localStorage.length; i++) {
		var usuari = localStorage.getItem('usuari' + i);
		var erradesStorage = localStorage.getItem('resultado' + i);
		if (erradesStorage != null && usuari != null) {
			llistaClassificacio.innerHTML += "<tr><td>" + usuari + "</td><td>" + erradesStorage + "</td></tr>"; 
		}
	}
}
