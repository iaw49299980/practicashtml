var didScroll;
var lastScrollTop = 0;
var delta = 5;
var navbarHeight = $('header').outerHeight();

$(window).scroll(function(event){
    didScroll = true;
});

setInterval(function() 
{
    if (didScroll) 
    {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() 
{
    var st = $(this).scrollTop();
    
    if(Math.abs(lastScrollTop - st) <= delta)
        return;
    
    if (st > lastScrollTop && st > navbarHeight)
    {
        $('header').css("top","-73.33px");
    } 
    else 
    {
        if(st + $(window).height() < $(document).height()) 
        {
            $('header').css("top","0px");
        }
    }
    
    lastScrollTop = st;
}

function toggleMenu()
{
    var leftMenu = $("#offCanvas").css("left");
    var position = leftMenu.substring(0,1);

    if(position=="-")
    {
        $("#offCanvas").animate({
            left:"0%"
        },500);
        $("#contentWeb").animate({
            left:"41.176%"
        },500);
    }
    else
    {
        $("#offCanvas").animate({
            left:"-41.176%"
        },500);
        $("#contentWeb").animate({
            left:"0%"
        },500);
    }
}