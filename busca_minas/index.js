/*
 * JS        20/03/2019
 *
 * Busca minas
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */
 // Inicialització de variables globals
 var mare; // apuntará a l'html capsa mare
 var capses; // Array que guardará el valor per cada posició del taulell
 var minesInput; // Apuntará a l'HTML input on es dirà quantes mines ha marcat l'usuari
 var minesInputValue = 0; // Comptarà la quantitat de mines que ha marcat l'usuari
 var minesMarcadesCorrectes = 0; // Mines que marca l'usuari i està correctament marcat
 var expandint = false; // Quan sigui true voldrà dir que l'usuari ha pulsat una casella blanca i s'està expandint
 var elementsComprovats = []; // Array que guardarà els elements comprovats en una expansió de blancs
 var perComprovar = []; // Array que guardarà aquells elements que haurem de comprovar en una expansió de blancs
 var width;; // Capses tamany
 var height;
 // Assignació de valors a les variables globals i Inicialització de la partida
 function ini()
 {
   mare = document.getElementById("mare");
   document.getElementById("manejador").innerHTML = "<label>Mines marcades: </label><input type='text' value = '' id='minesInput' disabled><br><label>D'un total de: </label><input type='text' value = '0' id='mines' disabled><br><button onclick='ini()'>Reiniciar</button>";
   minesInput = document.getElementById("minesInput");
   mare.innerHTML = '';
   minesInputValue = 0;
   minesInput.value = minesInputValue;
   rows = parseInt(prompt('Quantes files vols?'));
   columns = parseInt(prompt('Quantes columnes vols?'));
   mines = parseInt(prompt('Quantes mines vols?'));
   document.getElementById("mines").value = mines;
   taulell(rows, columns, mines);
   /*Dibuix del taulell*/
   div = "";
   width = 100 / capses[0].length;
   height = 100 / capses.length;
   for(i = 0; i < capses.length; i++)
   {
	   for (j = 0; j < capses[0].length; j++)
	   {
		   div += "<div class='capsaPetita' id='"+ i + "_" + j +"' style='width:"+width+"%;height:"+height+"%' onmousedown='pre_obrir(event, this)'></div>";
	   }
		div += "<div style='clear:both'></div>";
   }
   mare.innerHTML = div;
 }

/*Creació del array amb els valors per a cada casella*/
function taulell (files, columnes, mines)
{
  // Fem l'array amb la mida que ens ha donat l'usuari
  capses = makeArray(files, columnes);
  // Possem totes les mines al taulell
  for (i = 0; i < mines; i++)
  {
    colocaMinaAleatoria();
  }
  // Una vegada colocades les mines, possem els valors de cada casella
  colocaValorsCaselles();
}

// Crea un array bidimendional amb un tamany predefinit
function makeArray(files, columnes)
{
   var arr =  new Array(files);
   for(let i = 0; i < files; i++) {
     //arr.push(new Array(d2));
     arr[i]=new Array(columnes);
     
   }
   return arr;
 }

// Coloca les mines de forma aleatoria
function colocaMinaAleatoria()
{
  rowsRandom = Math.floor(Math.random() * capses.length);
  columnRandom = Math.floor(Math.random() * capses[0].length);
  // Si toca una posició on ja hi havia una mina tornem a cridar a la funció
  capses[rowsRandom][columnRandom] == 'M' ? colocaMinaAleatoria() : capses[rowsRandom][columnRandom] = 'M';
}
// Funció que coloca el valor per a cada casella;
function colocaValorsCaselles()
{
  for (var i = 0; i < capses.length; i++)
  {
    for (var j = 0; j < capses[0].length; j++)
    {
      // Si no es una casella on hi ha una mina, crida a la funcio que calcula el valor d'aquella casella
      capses[i][j] == 'M' ? false : capses[i][j] = calculaValorsCasella(i, j);
    }
  }

}
// Funció que retorna un array amb les posicions del voltant d'una casella
function alvoltantCasella(row, column)
{
  return [{x:row-1,y:column-1},
   {x:row-1,y:column},
   {x:row-1,y:column+1},
   {x:row,y:column-1},
   {x:row,y:column+1},
   {x:row+1,y:column-1},
   {x:row+1,y:column},
   {x:row+1,y:column+1}];
}
// Funció que calcula el valor d'una casella en funció de les mines que hi ha al voltant
function calculaValorsCasella(row, column)
{
  comptador = 0; // COmptador de mines que té la casella en concret
  alvoltantCasellaVar = alvoltantCasella(row, column); // array amb les posicions del voltant d'una casella
  // Recorrem les caselles que hi ha al voltant de la casella que estem analitzant en concret
   for (i = 0; i < alvoltantCasellaVar.length; i++)
   {
     // Si la posició està dins de l'array
     if (alvoltantCasellaVar[i].x >= 0 && alvoltantCasellaVar[i].x < capses.length &&
         alvoltantCasellaVar[i].y >= 0 && alvoltantCasellaVar[i].y < capses[0].length)
     {
       // Comprovem si es una mina, si ho és afeguim un al comptador
       if(capses[alvoltantCasellaVar[i].x][alvoltantCasellaVar[i].y] == 'M')
       {
   			comptador++;
   		 }
     }
   }
   // Retornem el número de mines, si es 0 retornem B de blanc
   return comptador == 0 ? 'B' : comptador;
}
// Funció que comprova si l'usuari a fet click dret o esquerra
function pre_obrir(event, element)
{
  // Si ha fet click dret crida a la funció on dibuixarà o esborrarà la bandera que marca que hi ha una mina
	if (event.button == 2)
	{
		marcar_bomba(element);
	}
  // Si es click esquerra comprovem el valor de l'element cridant la funció obrir
	else
	{
		obrir(element);
	}
}

// Funció que dibuixa o esborra la bandera que marca l'usuari per dir que hi ha una mina
function marcar_bomba (element)
{
	// Agafem la columna com la fila de l'id de l'element clickat
	column = parseInt(element.id.substring(element.id.indexOf("_") + 1, element.id.length));
	row = parseInt(element.id.substring(0, element.id.indexOf("_")));
	valorElement = capses[row][column]; // Agafem el valor de la posició de l'array
	if (element.innerHTML == '') 
	{
		element.innerHTML = 'M';
		element.classList.add('flag');
		minesInputValue++;
		valorElement == 'M' ? minesMarcadesCorrectes++ : false;
	}
	else if (element.innerHTML == 'M')
	{
		element.innerHTML = '';
		element.classList.remove('flag');
		minesInputValue--;
		valorElement == 'M' ? minesMarcadesCorrectes-- : false;
	}
	minesInput.value = minesInputValue;
	// Si les mines marcades correctes son tantes com mines hi ha i les mines marcades 
	// són igual a les correctes.
	// Vol dir que ha guanyat
	minesMarcadesCorrectes == mines && minesMarcadesCorrectes == minesInputValue ? mostra_solucio(guanyat=true) : false;
}
// Funció que comprova el valor de l'element clickat per l'usuari
function obrir(element)
{
	// Agafem la columna com la fila de l'id de l'element clickat
	column = parseInt(element.id.substring(element.id.indexOf("_") + 1, element.id.length));
	row = parseInt(element.id.substring(0, element.id.indexOf("_")));
	valorElement = capses[row][column]; // Agafem el valor de la posició de l'array
	if (valorElement == 'B')
	{
    // Si es una posició buida, cridem la funció que ens obrirà tots els blancs i números del voltant
		obrir_zona_blanca(element);
	}
	else if (valorElement == 'M')
	{
    // Si es una mina, comuniquem a l'usuari que ha perdut i mostrem la solució
		element.innerHTML = 'M';
		element.classList.add('mine');
		alert('Has perdut');
		mostra_solucio();
	}
	else
	{
    // Si es un número, el mostrem
		element.innerHTML = valorElement;
		element.classList.add('white');
	}
}
/*Funció que quan l'usuari clicka en una casella buida, expandeix aquesta a totes les buides i números del voltant*/
function obrir_zona_blanca(element)
{
   element.innerHTML = 'B'; // Mostrem el valor de la casella
   element.classList.add('white');
  elementsComprovats.push(element); // Afeguim l'element a la llista de caselles comprovades
  // L'array per comprovar, conté aquelles caselles buides que estàn al voltant de la que estem estudiant
  // i encara no han estat comprovades, es a dir, no están a l'array elements comprovats.
  expandint ? perComprovar.shift() : false; // Si estem en mode expandint, borrem de la llista per comprovar el primer element ja que es el que estem comprovant
	column = parseInt(element.id.substring(element.id.indexOf("_") + 1, element.id.length));
	row = parseInt(element.id.substring(0, element.id.indexOf("_")));
  // Agafem les caselles que están al voltant de la que estem comprovant
  alvoltantCasellaVar = alvoltantCasella(row, column);
  // Anem casella per casella del voltant de la que estem comprovant
	for (i = 0; i < alvoltantCasellaVar.length; i++)
  {
    // Si la casella que volem comprovar està dins de l'array comprovem que valor conté
    if (alvoltantCasellaVar[i].x >= 0 && alvoltantCasellaVar[i].x < capses.length &&
        alvoltantCasellaVar[i].y >= 0 && alvoltantCasellaVar[i].y < capses[0].length)
    {
      actualElement = document.getElementById(alvoltantCasellaVar[i].x+"_"+alvoltantCasellaVar[i].y);
      if(capses[alvoltantCasellaVar[i].x][alvoltantCasellaVar[i].y] == 'B')
      {
        // Si es un element buit, el marquem com a tal
  			actualElement.innerHTML = capses[alvoltantCasellaVar[i].x][alvoltantCasellaVar[i].y];
        // Comprovem si es un element que ja ha estat comprovat, si no ho ha estat el ficarem dins de l'array per comprovar
        estaComprovat(actualElement);
  		}
      else if (capses[alvoltantCasellaVar[i].x][alvoltantCasellaVar[i].y] != 'M')
      {
        // Si es un numero, mostrem el valor
        actualElement.innerHTML = capses[alvoltantCasellaVar[i].x][alvoltantCasellaVar[i].y];
		actualElement.classList.add('white');
      }

    }
  }
  // Mentre quedin elements per comprovar anem obrint la zona blanca
  while ( 0 < perComprovar.length) {
    expandint = true; // Expandint mode on
    obrir_zona_blanca(perComprovar[0]);
  }
}
// Funció que comprova si una casella ha estat comprovada en una recursió de blancs
function estaComprovat(element)
{
  trobada = false;
  for (var i = 0; i < elementsComprovats.length & !trobada; i++) {
    if (element.id == elementsComprovats[i].id)
    {
      trobada = true;
    }
  }
  for (var i = 0; i < perComprovar.length & !trobada; i++) {
    if (element.id == perComprovar[i].id)
    {
      trobada = true;
    }
  }
  // Si no ha estat comprovada ni esta a la llista de per comprovar, l'afeguim
  trobada ? false : perComprovar.push(element);
  return trobada;
}
// Funció que mostra el taulell amb tots els resultats
function mostra_solucio(guanyat=false)
{
	mare.innerHTML = '';
	div = '';
	for(i = 0; i < capses.length; i++)
	{
	   for (j = 0; j < capses[0].length; j++)
	   {
		   div +=  "<div style='width:"+width+"%;height:"+height+"%' class='capsaPetita white ";
		   capses[i][j] == 'M' && !guanyat ? div += "mine'" : false;
		   capses[i][j] == 'M' && guanyat ? div += "flag'" : false;
		   div += " id='"+ i + "_" + j +"'>"+capses[i][j]+"</div>";
		   
		 }
     div += "<div style='clear:both'></div>";
   }
   mare.innerHTML += div;
   resultat = document.getElementById("manejador");
   div = "<div class='";
   guanyat ?   div += "guanyat'><p>Has guanyat</p>" : div += "perdut'><p> Has perdut</p>";
   div += "<button onclick='ini()'>Reiniciar</button></div>";
   resultat.innerHTML = div;
}
