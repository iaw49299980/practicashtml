/*
 * JS        08/11/2018
 *
 * Programa que fa el funcionament del joc qui és qui
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */
var imatges;
var idAnterior; // Iniciem la variable que en guardará la id anterior
var parella = 0; // Iniciem la variable que ens dirá si em clickat en dues imatges o només portem una.
var errades = 0; // Iniciem la variable que ens portará les errades.
var encerts = 0; // Iniciem la variable que ens portará els encerts
var contador = localStorage.length; // Iniciem la variable de classificació.
var timer = 0; // Iniciem la variable del temps
var timerInterval;
var numCapses;
var dificultat;
/**
 *
 * Funció que al fer load a la pàgina reinicia el contador d'errades i encerts.
 *
 */
function marcador() {
	clearTimeout(timerInterval);
	document.getElementById('encerts').value = 0;
	document.getElementById('errades').value = 0;
}
/**
 *
 * Funció que aumenta els segons.
 *
 */
function aumentaSegons(){
	document.getElementById("timerP").innerHTML = timer;
	timer++;
	timerInterval = setTimeout(function(){aumentaSegons()}, 1000)
}
/**
 *
 * Funció que segons la dificultat carrega un numero de capses (parelles)
 *
 */
function carregaCapses(dif) {
	numCapses = dif;
	contenedor = document.getElementById("contenedor");
	for ( i = 0; i < numCapses; i++) {
		contenedor.innerHTML += '<div class="caja" onclick="cambiaImatge(' + i + ')" ondblclick="visualitzaImatge(' + i + ')"><img></div>'

	}
}
/**
 *
 * Funció que al fer load a la pàgina ordena random les imatges.
 *
 */
function carregaImatges() {
	dificultat = prompt("Quina dificultat vols?\n - facil \n - normal \n - dificil");

	if (dificultat == "facil")
	{
		imatges = ["img/papellona1.jpg", "img/papellona2.jpg", "img/papellona4.jpg","img/papellona1.jpg",
								"img/papellona2.jpg",  "img/papellona4.jpg", "img/papellona3.jpg",  "img/papellona3.jpg"];
		carregaCapses(8);
	}
	else if (dificultat == "normal")
	{
		imatges = ["img/papellona1.jpg", "img/papellona2.jpg", "img/papellona4.jpg","img/papellona1.jpg",
								"img/papellona2.jpg",  "img/papellona4.jpg", "img/papellona3.jpg",  "img/papellona3.jpg",
							"img/papellona5.jpg", "img/papellona5.jpg", "img/papellona6.jpg", "img/papellona6.jpg"];
		carregaCapses(12);
	}
	else
	{
		imatges = ["img/papellona1.jpg", "img/papellona2.jpg", "img/papellona4.jpg","img/papellona1.jpg",
								"img/papellona2.jpg",  "img/papellona4.jpg", "img/papellona3.jpg",  "img/papellona3.jpg",
								"img/papellona5.jpg", "img/papellona5.jpg", "img/papellona6.jpg", "img/papellona6.jpg",
							"img/papellona7.jpg", "img/papellona7.jpg", "img/papellona8.jpg", "img/papellona8.jpg"];
		carregaCapses(16);
	}
	imatges.sort(function(a, b){return 0.5 - Math.random()});
	var divImg = document.getElementsByClassName("caja");
	for (var i = 0; divImg.length > i; i++) {
		var img = document.getElementsByTagName("img")[i];
		img.setAttribute('src',imatges[i]);
	}
}
/**
 *
 * Funció que donat l'id fa un display block a l'imatge clickada durant un petit temps
 *
 * @param id
 *
 */

function visualitzaImatge(id) {
	// Agafem l'element clickat
	if (parella % 2 != 0) {
		var img = document.getElementsByTagName("img")[id];
						img.style.display = "block";
		setTimeout(
					function() {
						img.style.display = "none";
					}, 400);
	}
}


/**
 *
 * Funció que donat l'id fa un display block a l'imatge clickada, i en cas de ser la segona
 * passa el valor de l'id actual i de l'anterior a la funció comprovarParella().
 *
 * @param id
 *
 */

function cambiaImatge(id) {
	// Agafem l'element clickat
	var img = document.getElementsByTagName("img")[id];
	img.style.display = "block";
	// Comprovem que no sigui el primer moviment de la partida, si no ho és
	// l'hi passem l'element clickat i l'anterior per comparar-los
	if (idAnterior == id) {
		img.style.display = "none";
		idAnterior = undefined;
		parella++;
	}
	else if (idAnterior != undefined) {
		comprovarParella(id, idAnterior);
		idAnterior = id;
	} else {
		parella++;
		idAnterior = id;
	}
}
/**
 *
 * Funció que donat l'id de les dues imatges a comparar, ens comprova que siguin iguals.
 * Si són iguals l'hi treu el event list als elements i suma un a encerts, també comprova
 * si portem em encertat totes. En cas contrari, suma una errada i amb un delay de 500 milisegons
 * torna invisible les dues imatges.
 *
 *
 * @param id
 * @param idAnterior
 *
 */
function comprovarParella(id, idAnterior) {
	parella++;
	if (parella % 2 == 0) {
		var img = document.getElementsByTagName("img")[id];
		var imgAnterior = document.getElementsByTagName("img")[idAnterior];
		if (img.src == imgAnterior.src) {
			var divImg = document.getElementsByClassName("caja")[id];
			var divAnterior = document.getElementsByClassName("caja")[idAnterior];
			divImg.removeAttribute("onclick");
			divImg.style.border="5px solid green";
			divAnterior.removeAttribute("onclick");
			divAnterior.style.border="5px solid green";
			encerts++;
			document.getElementById("encerts").value = encerts;
			if (encerts == numCapses/2) {
				clearTimeout(timerInterval);
				alert("Molt bé. Només has hagut de fer " + errades + " errades. En un temps de " + timer);
				document.getElementById("classificacio").style.display = "block";
				document.getElementById("contenedor").style.opacity = "0.5";
				document.getElementById("resultat").style.opacity = "0.5";
			}
		} else {
			errades++;
			document.getElementById("errades").value = errades;
			setTimeout(
				function() {
					img.style.display = "none";
					imgAnterior.style.display = "none";
				}, 400);
		}
	}
}
/**
 *
 * Funció que donat
 *
 *
 * @param input
 *
 */
function setResultat(input) {
	var usuari = document.getElementById("nom").value;
	var llistaClassificacio = document.getElementById("llistaClassificacio");
	localStorage.setItem('usuari' + contador, usuari);
	localStorage.setItem('resultado' + contador, errades);
	localStorage.setItem('timer' + contador, timer);
	localStorage.setItem('dificultat' + contador, dificultat);
	llistaClassificacio.innerHTML += "<h3>Resultats "+dificultat+"</h3>"
	llistaClassificacio.innerHTML += "<th>Usuari</th><th>Errades</th><th>Timer</th>"
	for (var i = 0; i < localStorage.length; i++) {
		usuari = localStorage.getItem('usuari' + i);
		erradesStorage = localStorage.getItem('resultado' + i);
		timerStorage = localStorage.getItem('timer' + i);
		dificultatStorage = localStorage.getItem('dificultat' + i);
		if (erradesStorage != null && usuari != null && dificultatStorage == dificultat) {
			llistaClassificacio.innerHTML += "<tr><td>" + usuari + "</td><td>" + erradesStorage + "</td><td>" + timerStorage + "</td></tr>";
		}
	}
}
