/*
* @author: Pablo Hernando
* @versio: 1.0
* @description: Creación y validación de datos de un currículum
* @date: 16/04-2018
*
*/
/*
 $(function(){
            //detectar scroll hacia abajo
            var obj = $(document);          //objeto sobre el que quiero detectar scroll
            var obj_top = obj.scrollTop()   //scroll vertical inicial del objeto
            obj.scroll(function(){
                var obj_act_top = $(this).scrollTop();  //obtener scroll top instantaneo
                if(obj_act_top > obj_top){
                    //scroll hacia abajo

                    $("#nav").css("display","block");

                }else{
                    //scroll hacia arriba

                    $("#nav").css("display","none");

                }
                obj_top = obj_act_top;                  //almacenar scroll top anterior
            });
        });
*/
jQuery(function($) {
  function fixDiv() {
    var nav = $('#nav');
    var padding = $("#news");
    if ($(window).scrollTop() > $("header").height()){
      padding.addClass("padding");
      nav.css({
        'position': 'fixed',
        'top': '0px',
        'width': '100%'
      });
      $(".toTop").show();
    }
    else{
      nav.css({
        'position': 'relative',
        'top': 'auto',
        'width': '100%'
      });
    padding.removeClass("padding");
    $(".toTop").hide();
  }}
  $(window).scroll(fixDiv);
  fixDiv();
});
$(document).ready(function() {
  $("#titleBcn").typeIt();
  $(".toNews").click(function() {
     $('html, body').animate({
         scrollTop: $("#toNews").offset().top
     }, 1000)});
  $(".toCalendar").click(function() {
          $('html, body').animate({
              scrollTop: $("#toCalendar").offset().top
          }, 1000);
      });
  $(".toMzb").click(function() {
     $('html, body').animate({
         scrollTop: $("#toMzb").offset().top
     }, 1000);
 });
 $(".toGalery").click(function() {
     $('html, body').animate({
         scrollTop: $("#toGalery").offset().top
     }, 1000);
 });
 $(".toContact").click(function() {
     $('html, body').animate({
         scrollTop: $("#toContact").offset().top
     }, 1000);
});
 $(".toTop").click(function() {
     $('html, body').animate({
         scrollTop: $("#imgPortada").offset().top
     }, 1000);
});
// Get the <span> element that closes the modal

});
// Get the modal

function openImg(img){
  var modal = document.getElementById('myModal');
  var modalImg = document.getElementById('img01');
    modal.style.display = "block";
    modalImg.src = img.src;
}
function closeImg(){
  $('#myModal').hide(500);
}
function openNav() {
  $(".menu").toggle(500);
}
function closeNav(){
  if($(window).width() < 1100){
    $(".menu").hide(500);
  }
}
