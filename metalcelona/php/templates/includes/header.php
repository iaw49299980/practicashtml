<!-- El head y el header para la mayoria de nuestros archivos -->
<!DOCTYPE html>
<html lang="es">
  <head>
    <title>PHM World</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <script type="text/javascript" src="../js/jqu.js"></script>
    <script type="text/javascript" src="../js/js.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/css.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../img/favicon.ico" type="image/x-icon">
  </head>
  <body>
    <div id="cajaMenu">
      <img src="../img/menu.png" id="imgMenu" alt="Imagen Menú Responsive PHMMY" onclick="desplegarMenu()">
      <nav class="noseleccionable">
        <ul id="listaNiveles">
          <li>
              <?php echo  "<a href='login.php?usuario=".$_GET['usuario']."'>Home (Blog)</a>" ?>
          </li>
          <li>
              <?php echo  "<a href='perfil.php?usuario=".$_GET['usuario']."'>Profile</a>" ?>
          </li>
        <li>Grammar &#8675;</li>
        <li>
        <?php echo  "<a href='login.php?usuario=".$_GET['usuario']."'>" ?>Add Topics</a>
        </li>
        <li>
          <span onclick="desplegarGramVocB()">Your Topics &#8675;</span>
              <ul id="grammarB">
                <?php include "templates/functions/temasLista.php" ?>

              </ul>
        </li>
        <li>
          <?php
           echo  "<a href='index.php'>Log Out</a>"
           ?>
        </li>
        </ul>
      </nav>
    </div>
