<?php
//Include the event calendar functions file
include_once('functions.php');
?>
<!DOCTYPE html>
<html lang="es" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Metalcelona</title>
    <link href="https://fonts.googleapis.com/css?family=Cinzel|Cormorant+Unicase|Stoke" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
    <script type="text/javascript" src="js/jquery/jquery-3.3.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.typeit/3.0.1/typeit.min.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <link rel="shortcut icon" href="img/horns.jpg" />
  </head>
  <body>
    <header>
    	<div id="imgPortada">
          <div class="divFlexHeader noSeleccionable">
          	<h1 class="titleHeader">
              <span id="titleMetalcelona">
                Metalcelona
                <br>
                Tu web de metal en
                <br>
              </span>
              <span id="titleBcn">
                Barcelona
              </span>
            </h1>
    	      <h3 class="titleHeader"></h3><br>
        </div>
      </div>
    </header>
    <nav id="nav">
      <div class="logoNav">
        <h1 class="noSeleccionable">
          <a href="#" style="color:white">
            METACELONA
          </a>
        </h1>
      </div>
      <div class="navResponsive">
        <img src="img/menu3.png" onclick="openNav()"></img>
      </div>
      <ul class="menu">
        <li class="mainLi toNews">
          <a href="#news">
            Notícias
          </a>
        </li>
        <li class="mainLi toCalendar">
          <a href="#calendar">
            Calendario
          </a>
        </li>
        <li class="mainLi toMzb" id="menuZone">
            <a href="#mzb">
              Metal Zone Barcelona
            </a>
          <ul id="subMenuZone">
            <li>
              <a href="#halls">
                Salas
              </a>
            </li>
            <li>
              <a href="#bars">
                Bares
              </a>
            </li>
            <li>
              <a href="#locals">
                Locales
              </a>
            </li>
          </ul>
        </li>
        <li class="mainLi toGalery">
          <a href="#galery">
            Galería
          </a>
        </li>
        <li class="mainLi toContact">
          <a href="#contact">
            Contacto
          </a>
        </li>
      </ul>
    </nav>
    <article onclick="closeNav()" class="">
      <span id="toNews"></span>
      <section id="news">
        <div class="titleSection">
          <h2>Notícias</h2>
        </div>
          <div class="flexNews">
            <div class="mainNew">
		          <h3 style="margin:20px 0px;">Notícia de interés</h3>
              <div class="imageMainNew">
                  <img src="https://i.axs.com/2014/09/flogging-molly_09-09-14_20_540f476d6529a.jpg">
              </div>
              <div class="textMainNew">
                <h3 id="titleMainNew">
                  Flogging Molly en Resurrection Tour
                </h3>
                <p id="textMainNew">
                 Resurrection Tour nos trai la novedad tras 10 años de la llegada de la banda de punk folk irlandesa Flogging Molly. Anunciadas 3 fechas en diferentes ciudades españolas nos prometen una gran velada y unica para recordar con su gran musica.
                </p>
                <a href="#">Continuar leyendo</a>
              </div>
            </div>
          <div class="otherNews">
            <div class="otherNew">
              <h3 style="margin:20px 0px;">Otras notícias</h3>
              <img src="http://img2.rtve.es/i/?w=1600&i=1537782750906.jpg">
              <h4 class="titleOtherNew">
                Metallica regresa en Marzo de 2019
              </h4>
              <div style="clear:both;"></div>
            </div>
            <div class="otherNew">
              <img src="https://www.zona-zero.net/images/grupos/d/disturbed/2018_Disturbed.jpg">
              <h4 class="titleOtherNew">
                Disturbed regresa a España en Abril de 2019
              </h4>
              <div style="clear:both;"></div>
            </div>
            <div class="otherNew">
              <img src="http://www.hellpress.com/wp-content/uploads/2017/07/nightrage-the-venomous.jpg">
              <h4 class="titleOtherNew">
                 Gran bolo de Nightrage
              </h4>
              <div style="clear:both;"></div>
            </div>
            <div class="otherNew">
              <img src="https://zona.cocacola.es/19201201/zona/post_images/standard/detail_sd8OcQao1TkpfAw6uPeZSyMFDhb9nv.jpg">
              <h4 class="titleOtherNew">
                 Be Prog! My Friend nos dice adios.
              </h4>
              <div style="clear:both;"></div>
            </div>
            <div class="otherNew">
              <img src="https://tixa.hu/kepek/0003/768/3747-1.jpg">
              <h4 class="titleOtherNew">
                The Rumbjacks vuelve a la ciudad condal.
              </h4>
              <div style="clear:both;"></div>
            </div>

          </div>
        </div>

      </section>
      <span id="toCalendar"></span>
      <section id="calendar">
        <div class="titleSection">
          <h2>Calendario</h2>
        </div>
        <div id="calendar_div">
          <?php echo getCalender(); ?>
        </div>
      </section>
      <span id="toMzb"></span>
      <section id="mzb">
        <div class="titleSection">
          <h2>Metal Zone Barcelona</h2>
        </div>
      </section>
      <span id="toHalls"></span>
      <section id="halls">
        <div class="contentSection">
          <div class="titleSection">
            <h3>Salas</h3>
          </div>
        </div>
      </section>
      <span id="toBars"></span>
      <section id="bars">
        <div class="titleSection">
          <h3>Bares</h3>
        </div>
      </section>
      <span id="toLocals"></span>
      <section id="locals">
        <div class="titleSection">
          <h3>Locales</h3>
        </div>
      </section>
      <span id="toGalery"></span>
      <section id="galery">
        <div class="titleSection">
          <h2>Galeria</h2>
        </div>
          <div class="galleryFlex">
            <div class="gallery">
              <img src="https://i.axs.com/2014/09/flogging-molly_09-09-14_20_540f476d6529a.jpg">
              <div class="desc">
                Add a description of the image here
              </div>
            </div>
            <div class="gallery">
              <img src="http://img2.rtve.es/i/?w=1600&i=1537782750906.jpg">
              <div class="desc">
                Add a description of the image here
              </div>
            </div>
            <div class="gallery">
              <img src="https://www.zona-zero.net/images/grupos/d/disturbed/2018_Disturbed.jpg">
              <div class="desc">
                Add a description of the image here
              </div>
            </div>
            <div class="gallery">
              <img src="http://www.hellpress.com/wp-content/uploads/2017/07/nightrage-the-venomous.jpg">
              <div class="desc">
                Add a description of the image here
              </div>
            </div>
            <div class="gallery">
              <img src="https://zona.cocacola.es/19201201/zona/post_images/standard/detail_sd8OcQao1TkpfAw6uPeZSyMFDhb9nv.jpg">
              <div class="desc">
                Add a description of the image here
              </div>
            </div>
            <div class="gallery">
              <img src="https://tixa.hu/kepek/0003/768/3747-1.jpg">
              <div class="desc">
                Add a description of the image here
              </div>
            </div>
            <div id="myModal" class="modal">

              <!-- The Close Button -->
              <span class="close" onclick="closeImg()">&times;</span>

              <!-- Modal Content (The Image) -->
              <img class="modal-content" id="img01">

          </div>
      </section>
      <span id="toContact"></span>
      <section id="contact">
        <div class="titleSection">
          <h2>Contacto</h2>
        </div>
      </section>
    </article>
    <div class="toTop">
        <a href="#imgPortada">Aqui imagen Top</p>
    </div>
    <footer>

    </footer>
  </body>
</html>
