/*
 * JS        20/03/2019
 *
 * Barcos
 *
 * Copyright 2018 Pablo Hernando <gayarre.pablo@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

var mare;
var barcos;
var barcosMarcats;
var barcosMarcatsInput;
var barcosInput;
var files;
var columnes;
var widthCapsa;
var heightCapsa;
var arrayCapses;
var capsesClass;

function ini()
{
	mare = document.getElementById('mare');
	barcosMarcats = 0;
	barcosMarcatsInput = document.getElementById('barcosInput');
	barcosMarcatsInput.value = barcosMarcats;
	barcosInput = document.getElementById('barcos');
	barcos = 6;
	barcosInput.value = barcos;
	files = 8;
	columnes = 6;
	height = 100 / files;
	width = 100 / columnes;
	taulell();
	ferArray();
  colocaVaixells();
}

function taulell()
{
	var div2inner = "";
	for (i = 0; i < files; i++)
	{
		for (j = 0; j < columnes; j++)
		{
			div2inner+='<div onmousedown="pre_obrir(event, this)" class="capsaPetita" id="'+i+'_'+j+'" style="height:'+height+'%;width:'+width+'%;" ></div>';

		}
		div2inner+='<div style="clear:both;"></div>';
	}
	mare.innerHTML = div2inner;
}
function ferArray()
{
	arrayCapses = new Array(files);
	for (i = 0; i < files; i++)
	{
		arrayCapses[i] = new Array(columnes);
	}
}

function vaixellAleatori()
{
  switch (Math.floor(Math.random() * 5))
  {
    case 0:
      return [[1, 1, 1]];
      break;
    case 1:
      return [[1]];
      break;
    case 2:
      return [[1,1]];
      break;
    case 3:
      return [[1],[1]];
      break;
    default:
      return [[1], [1], [1]];
      break;
  }
}

function colocaVaixells()
{
  for (index = 0; index < barcos; index++)
  {
    vaixell = vaixellAleatori();
    posicioX = Math.floor(Math.random() * files);
    posicioY = Math.floor(Math.random() * columnes);
    compX = posicioX + vaixell.length;
    compY = posicioY + vaixell[0].length;
    compX > arrayCapses.length ? posicioX -= vaixell.length : false;
    compY > arrayCapses[0].length ? posicioY -= vaixell[0].length : false;
    vaixellAlVoltant(posicioX, posicioY, vaixell) ? --index : colocaVaixell(posicioX, posicioY, vaixell);
  }
}
function vaixellAlVoltant(posicioX, posicioY, vaixell)
{
  auxY = posicioY;
  for (j = 0; j < vaixell.length; j++, posicioX++)
  {
    for (k = 0, posicioY = auxY; k < vaixell[0].length; k++, posicioY++)
    {
      alvoltantCasellaVar = alvoltantCasella(posicioX, posicioY); // array amb les posicions del voltant d'una casella
      // Recorrem les caselles que hi ha al voltant de la casella que estem analitzant en concret
       for (i = 0; i < alvoltantCasellaVar.length; i++)
       {
         // Si la posició està dins de l'array
         if (alvoltantCasellaVar[i].x >= 0 && alvoltantCasellaVar[i].x < arrayCapses.length &&
             alvoltantCasellaVar[i].y >= 0 && alvoltantCasellaVar[i].y < arrayCapses[0].length)
         {
           // Comprovem si es una mina, si ho és afeguim un al comptador
           if(arrayCapses[alvoltantCasellaVar[i].x][alvoltantCasellaVar[i].y] == 1)
           {
             return true;
           }
         }
       }
    }
  }
  return false;
}
function alvoltantCasella(row, column)
{
  return [{x:row-1,y:column-1},
   {x:row-1,y:column},
   {x:row-1,y:column+1},
   {x:row,y:column-1},
   {x:row,y:column+1},
   {x:row+1,y:column-1},
   {x:row+1,y:column},
   {x:row+1,y:column+1}];
}
function colocaVaixell(posicioX, posicioY, vaixell)
{
  auxY = posicioY;
  for (j = 0; j < vaixell.length; j++, posicioX++)
  {
    for (k = 0, posicioY = auxY; k < vaixell[0].length; k++, posicioY++)
    {
      arrayCapses[posicioX][posicioY] = vaixell[j][k];
      //document.getElementById(posicioX + "_" + posicioY).style.backgroundColor = "red";
    }
  }
}// Funció que comprova si l'usuari a fet click dret o esquerra
function pre_obrir(event, element)
{
  // Si ha fet click dret crida a la funció on dibuixarà o esborrarà la bandera que marca que hi ha una mina
	if (event.button == 2)
	{
		marcar_aigua(element);
	}
  // Si es click esquerra comprovem el valor de l'element cridant la funció obrir
	else
	{
		marcar_vaixell(element);
	}
}

function marcar_vaixell(element)
{
  posX = parseInt(element.id.substring(element.id.indexOf("_") + 1, element.id.length));
	posY = parseInt(element.id.substring(0, element.id.indexOf("_")));
	valorElement = arrayCapses[posX][posY]; // Agafem el valor de la posició de l'array
	if (valorElement == 1)
	{
		element.style.backgroundColor = "green";
	}
	else
	{
		element.style.backgroundColor = "red";
	}
}
function marcar_aigua(element)
{
  posX = parseInt(element.id.substring(element.id.indexOf("_") + 1, element.id.length));
	posY = parseInt(element.id.substring(0, element.id.indexOf("_")));
	valorElement = arrayCapses[posX][posY]; // Agafem el valor de la posició de l'array
	if (valorElement != 1)
	{
		element.style.backgroundColor = "blue";
	}
	else
	{
		element.style.backgroundColor = "red";
	}
}
