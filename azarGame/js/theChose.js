/*
 * @author: Pablo Hernando
 * @versio: 1.0
 * @date: 16/04-2018
 *
 */
var position;
var randomLength;
var boxes;
var iteration;
var animationVar;

function openBet(chose) {
			$(".scoreTheChoseTitle").html("Playing...");
			theChose(chose);
}

function theChose(chose) {
	clearIntervalA();
	boxes = document.getElementsByClassName("numberTheChose");
	randomLength = Math.round(Math.random() * 20);
	position = 0;
	for (iteration = 0; iteration <= boxes.length; iteration++) {
		$($(".numberTheChose")[iteration]).css({
			"background-color": "orange",
			"color": "#0074D9"
		});
	}
	iteration = 0;
	animationVar = window.setInterval(function () {
		animation(chose);
		position++;
		iteration++;
		if (position == boxes.length + 1) {
			$($(".numberTheChose")[position - 1]).css({
				"background-color": "orange",
				"color": "#0074D9"
			});
			position -= 11;
		}
	}, 500)
}

function animation(chose) {
	$($(".numberTheChose")[position]).css({
		"background-color": "#0074D9",
		"color": "orange"
	});
	if (position > 0) {
		$($(".numberTheChose")[position - 1]).css({
			"background-color": "orange",
			"color": "#0074D9"
		});
	}
	if (iteration == randomLength) {
		clearInterval(animationVar);
		if (position == 5 && chose == "equal") {
			$(".scoreTheChoseTitle").html("You win");
		} else if (position > 5 && chose == "greater") {
			$(".scoreTheChoseTitle").html("You win");
		} else if (position < 5 && chose == "greater") {
			$(".scoreTheChoseTitle").html("You Lose");
		} else if (position < 5 && chose == "lesser") {
			$(".scoreTheChoseTitle").html("You win");
		} else {
			$(".scoreTheChoseTitle").html("You Lose");
		}
	}
}

function clearIntervalA() {
	clearInterval(animationVar);
	position = 0;
	iteration = 0;
}
