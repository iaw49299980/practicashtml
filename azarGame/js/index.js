/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
*
*/
function load() {
  document.getElementById("input").value="";
}
var answerDoIt = [
                  "Today is gonna be a good day",
                  "The only man who never makes mistakes is the man who never does anything.",
                  "What would life be if we had no courage to attempt anything?",
                  "There's doubt in trying. Just do it or stop thinking",
                  "Sometimes you just do what you gotta do to get through them." ,
                  "Twenty years from now you will be more disappointed by the things that you didn’t do than by the ones you did do.",
                  "Everybody is different. Everybody has different styles. Just do it the best way you know how.",
                  "If you think you can, you can.",
                  "Have a robust mindset; dare to move any mountain!",
                  "If you can dream it, you can do it.",
                  "You don't need anyone to believe that you can do it, as long as you believe in yourself and what you're capable of.",
                  "When you believe that you can do something, that's when you can do it. ",
                  "Do the thing and you will have the power.",
                  "The whole secret of a successful life is to find out what is one's destiny to do, and then do it."
                ];
var answerDontDoIt = [
                      "Only bad things can become of that decision.",
                      "The Graveyards Are Full of Brave Men.",
                      "Why don't you just surrender?",
                      "You are a simple man.",
                      "Imagine smiling after a slap in the face. Then think of doing it twenty-four hours a day.",
                      "Don't cry over someone who wouldn't cry over you.",
                      "Nothing can cure the soul but the senses, just as nothing can cure the senses but the soul.",
                      "When you've suffered a great deal in life, each additional pain is both unbearable and trifling."
                    ];
var respondeDoIt = [
                    "Siempre parece imposible hasta que se hace.",
                    "La buena fortuna favorece a los atrevidos.",
                    "El mejor placer de la vida es hacer las cosas que la gente dice que no podemos hacer.",
                    "Este es tu momento.",
                    "La felicidad no es algo hecho. Proviene de tus propias acciones.",
                    "El único lugar en que el éxito viene antes que el trabajo es en el diccionario.",
                    "Hoy va a ser tu gran día.",
                    "Te están esperando nuevo cambios.",
                    "La vida puede ser asombrosa.",
                    "Solo cosas buenas pueden pasarte de está decisión."
                  ];
var respondeDontDoIt = [
                        "El cementerio está lleno de valientes.",
                        "Esto puede ser extremadamente malo.",
                        "Eso no es para ti.",
                        "Solo cosas malas pueden pasarte de está decisión."
                       ];
var answered = false;
var globalLanguage = "en";
var destroyAnimation;
/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description: Validate that has some text.
* @return boolean
*/
function confirmQuestion(value) {
  $("#bigAnswer").html("Ask Me!");
  $("#circleAsk").css({
    "background-color":"#0074D9",
    "border-bottom": "solid 3px #0074D9",
    "box-shadow": "0px 2px 2px rgba(0, 0, 0, 0.49"
  });
  if (value.value != "") {
      document.getElementById("circleAsk").addEventListener("click", randomAnswer);
      document.getElementById("circleAsk").removeEventListener("click", addQuestion);
  } else {
      document.getElementById("circleAsk").removeEventListener("click", randomAnswer);
      document.getElementById("circleAsk").addEventListener("click", addQuestion);
  }
}
/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description: Validate that has some text.
* @return boolean
*/
function addQuestion() {
  $("#divAnswer").css({
    "padding": "20px"
  });
  $("#why").hide(500);
  if (globalLanguage == "en") {
    if (answered) {
      $("#answer").html("Add a new question!");
      $("#bigAnswer").html("Ask me!");
      $("#circleAsk").css({
        "background-color":"#0074D9",
        "border-bottom": "solid 3px #0074D9",
        "box-shadow": "0px 2px 2px rgba(0, 0, 0, 0.49"
      });
    } else {
      $("#answer").html("Add a question!");
    }
  } else {
    if (answered) {
      $("#answer").html("¡Pregúntame algo nuevo!");
      $("#bigAnswer").html("¡Pregúntame!");
      $("#answer").html("Add a new question!");
      $("#bigAnswer").html("Ask me!");
      $("#circleAsk").css({
        "background-color":"#0074D9",
        "border-bottom": "solid 3px #0074D9",
        "box-shadow": "0px 2px 2px rgba(0, 0, 0, 0.49"
      });
    } else {
      $("#answer").html("¡Añade una cuestión!");
    }
  }
}
/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description: Change Language.
*/
function changeLanguage(language) {
  if (language == "en") {
      globalLanguage = "en";
      $("#input").attr("placeholder", "Put your question:");
      $("#bigAnswer").html("Ask me!");
      $("#why").html("Why?");
      $("#title").html('"Like your friend\'s advice but better"');
  } else {
      globalLanguage = "es";
      $("#input").attr("placeholder", "Pon tu pregunta:");
      $("#bigAnswer").html("¡Pregúntame!");
      $("#why").html("¿Por que?");
      $("#title").html('"Mejor que cualquier otro consejo"');
    }
}

/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description: Decides randomly if the user has to do it or don't do it.
* @return boolean
*/
function randomDecision() {
  var randomDecision = Math.round(Math.random() - 0.4);
  return randomDecision;
}
/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description: returns a random position.
* @return int
*/
function randomPosition(length) {
  var randomPosition = Math.floor(Math.random() * length);
  return randomPosition;
}
/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description: Decides randomly the answer.
* @return String
*/
function randomAnswer() {
  var varRandomDecision = randomDecision();
  var answer = "";
  if (globalLanguage == "en") {
    if (varRandomDecision == 0) {
        var i = randomPosition(answerDoIt.length);
        answer = answerDoIt[i];
    } else {
        var i = randomPosition(answerDontDoIt.length);
        answer = answerDontDoIt[i];
    }
  } else {
    if (varRandomDecision == 0) {
        var i = randomPosition(respondeDoIt.length);
        answer = respondeDoIt[i];
    } else {
        var i = randomPosition(respondeDontDoIt.length);
        answer = respondeDontDoIt[i];
    }
  }

  printAnswer(varRandomDecision, answer);
}
/*
* @author: Pablo Hernando
* @versio: 1.0
* @date: 16/04-2018
* @description:Load an answerDoIt or an answerDontDoIt.
*/
function printAnswer(varRandomDecision, answer) {
  animation();
  $("#answer").html(answer);
  $("#divAnswer").css({
    "padding": "20px"
  });
  $("#why").show(500);
  if (varRandomDecision == 0) {
    if (globalLanguage == "en") {
      $("#bigAnswer").html("DO IT!");
    } else{
      $("#bigAnswer").html("¡HAZLO!");
    }
    $("#circleAsk").css({
      "background-color":" #2ECC40",
      "border-bottom": "solid 3px rgba(80,204,100, 0.89)",
      "box-shadow": "0px 2px 2px rgba(0, 0, 0, 0.49"
    });
  } else {
    if (globalLanguage == "en") {
      $("#bigAnswer").html("DON'T DO IT!");
    } else{
      $("#bigAnswer").html("¡OLVÍDATE!");
    }
    $("#circleAsk").css({
      "background-color":" #ff8181",
      "border-bottom": "solid 3px #bd6565",
      "box-shadow": "0px 2px 2px rgba(0, 0, 0, 0.29)"
    });
  }
  document.getElementById("circleAsk").removeEventListener("click", randomAnswer);
  document.getElementById("circleAsk").addEventListener("click", addQuestion);
  answered = true;
}
function animation () {
  $("#circleAsk").css({
    "animation": "shake 0.82s cubic-bezier(.36,.07,.19,.97) both"
  });
  destroyAnimation = setInterval(destroyAnimation2, 830);
}
function destroyAnimation2() {
  $("#circleAsk").css({
    "animation": ""
  });
  clearInterval(destroyAnimation);
}
function microphone(){
  if (globalLanguage == "en") {
      speechRs.rec_start('en-US',function(final_transcript,interim_transcript){
      document.getElementById('input').value=final_transcript+interim_transcript;
      if (input.value != "") {
          document.getElementById("circleAsk").addEventListener("click", randomAnswer);
          document.getElementById("circleAsk").removeEventListener("click", addQuestion);
      } else {
          document.getElementById("circleAsk").removeEventListener("click", randomAnswer);
          document.getElementById("circleAsk").addEventListener("click", addQuestion);
      }
    });
  } else{
    speechRs.rec_start('es-ES',function(final_transcript,interim_transcript){
    document.getElementById('input').value=final_transcript+interim_transcript;
    if (input.value != "") {
        document.getElementById("circleAsk").addEventListener("click", randomAnswer);
        document.getElementById("circleAsk").removeEventListener("click", addQuestion);
    } else {
        document.getElementById("circleAsk").removeEventListener("click", randomAnswer);
        document.getElementById("circleAsk").addEventListener("click", addQuestion);
    }
  });
  }

}





var speechRs = speechRs || {};
speechRs.speechinit = function(lang,cb,bcolor,color,pitch,rate){
   this.speaker = new SpeechSynthesisUtterance();
   this.speaker.pitch=pitch || 1;
   this.speaker.rate=rate || 1;
   this.lan = lang;
   var style = document.createElement('style');
	style.type = 'text/css';
	style.innerHTML = '.rsClass{background-color:'+(bcolor || "#4f91e6")+';color:'+(color || "#fff")+';}';
	document.getElementsByTagName('head')[0].appendChild(style);
   setTimeout(function(){
   speechRs.speaker.voice = speechSynthesis.getVoices().
     filter(function(voice) {  return voice.name == speechRs.lan; })[0];
   },500);
   if(lang == 'native'){
	cb(this);
   }else{
     setTimeout(function(){
	 cb(speechRs)
	 },1000);
   }
  }

speechRs.speak = function(text,cb,isHiligh) {
	 let j=0,el,ar=[];
     speechRs.speaker.voice = speechSynthesis.getVoices().
     filter(function(voice) {  return voice.name == speechRs.lan; })[0];
	 this.speaker.onend = function(e) {
		cb(e);
    };
	if (typeof text == 'string') {
      this.speaker.text = text;
      speechSynthesis.speak(this.speaker);
   } else {
	   if(isHiligh){
		    j = 0;
			el = text;
		    ar = (text.innerHTML).split(".");
			readop(ar[j]);
		}else{
		  this.speaker.text = text.innerHTML;
        speechSynthesis.speak(this.speaker);
		}
   }

	function readop(x){
	  speechRs.speaker.text = x;
	  if(j != 0){
	  el.querySelector(".rsClass").className = "";
	  }
	  el.innerHTML = (el.innerHTML).replace(ar[j],"<span class='rsClass'>"+ar[j]+"</span>");
	  speechSynthesis.speak(speechRs.speaker);
	  speechRs.speaker.onend = function(e){
	     if(ar.length>(j+1)){
	      readop(ar[++j]);
		  }
	  }
	}
  }

speechRs.rec_start = function(l,callback){

	 this.recognition = new webkitSpeechRecognition();
	 this.recognition.continuous = true;
	 this.recognition.interimResults = true;
         this.arry_com = {};
	 this.final_transcript = '';
         this.recognition.lang = l;
	 this.recognition.start();
	 this.ignore_onend = false;
	 this.recognition.onstart = function(c) {

         }
	let prev_res='';
	this.recognition.onresult = function(event) {
			 let interim_transcript = '';
			    if (typeof(event.results) == 'undefined') {
			      speechRs.recognition.onend = null;
			      speechRs.recognition.stop();
			      return;
			    }

			    for (var i = event.resultIndex; i < event.results.length; ++i) {
			        if (event.results[i].isFinal) {
				   prev_res='';
			           speechRs.final_transcript += event.results[i][0].transcript;
			           } else {
			          interim_transcript += event.results[i][0].transcript;

                                 }
			     }
			  console.log(prev_res+","+interim_transcript);
			  if(prev_res != interim_transcript && speechRs.arry_com[interim_transcript.toLowerCase().trim()]){
			       prev_res = interim_transcript;
                               speechRs.arry_com[interim_transcript.toLowerCase().trim()]();

			  }else{
			  }

			    callback(speechRs.final_transcript.replace("undefined",""),interim_transcript);

		       }
}

speechRs.on = function(s,f){
	this.arry_com[s.toLowerCase()] = f;
}

speechRs.rec_stop = function(callback){
  this.recognition.stop();
  this.recognition.onstop = function() {
	 return callback();
  }
}
